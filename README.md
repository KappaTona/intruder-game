# Implement socket server-clien example
found in `man getaddrinfo` under the EXAMPLE section

# Intruder Game


## Description about the technologies
- The idea is, that I would like to practice C++ and learn more about socket based server client program using the UDP protocol, since it is the recommended way. For this to achieve I turn to the linux manual sections such as `man 2 socket`; `man 3 getaddrinfo`. They found locally on Arch linux after installing the [man-db](https://archlinux.org/packages/?name=man-db) and [man-pages](https://archlinux.org/packages/?name=man-pages).
- The GUI will be provided by [ncurses library](https://en.wikipedia.org/wiki/Ncurses)
for easy and fast realization.
- For two player mode I intent to use Qt5::Gamepad


## Description about the game
Given host string `\[T]/` is able to throw rocks into 4 direction
`o`.  Darkness envelopped characters `(>_<)` can spawn randomly using network. 
By default they are not approaching the target host 
Later on when they are moving: if they touched the host, the game is over.
If rock `o`  hits DEC `(>_<)` DEC disconnects from the game.
Multiple DEC can be present, A score of number is equal to the destroyed DECs


## Some example pictures or videos about the project conception
- One of the basic scene concept

![](./pics/basic.png)

- Example advanced scene

![](./pics/advanced.png)

## Installation [TODO]
Within a particular ecosystem, there may be a common way of installing things, such as using Yarn, NuGet, or Homebrew. However, consider the possibility that whoever is reading your README is a novice and would like more guidance. Listing specific steps helps remove ambiguity and gets people to using your project as quickly as possible. If it only runs in a specific context like a particular programming language version or operating system or has dependencies that have to be installed manually, also add a Requirements subsection.

## Usage [TODO]
Use examples liberally, and show the expected output if you can. It's helpful to have inline the smallest example of usage that you can demonstrate, while providing links to more sophisticated examples if they are too long to reasonably include in the README.

## Authors and acknowledgment
Gratitude towards to the Open Source Softwares:
- Arch Linux as a trusty abd fast OS,
- ncurses as a fast to learn hard to master GUI
- glibc for socket-server IMPL
- Qt5 for Gamepad usage
- CMake for making Makefiles isntead of me
- GTest for allowing quality

And I should remind myself what the truth sayers told me:
- Aron and Konya, as they told me that they have taught me all they could and the rest is upob me

## License
License is DO WHAT YOU WANT PUBLIC LICENSE [](./LICENSE)

## Project status
Work in progress.
- create the basic scene 
- create 2 player mode
- create enemies
- create netcode
