cmake_minimum_required(VERSION 3.21)
project(intruder-game)
set(CMAKE_EXPORT_COMPILE_COMMANDS ON)
set(CMAKE_CXX_STANDARD 17)

add_subdirectory(game_design)
add_executable(intruder_game main.cpp)
target_link_libraries(intruder_game projection)

option(BUILD_TEST "Option build unittests" OFF)
if(BUILD_TEST)
    enable_testing()
    include(FetchContent)
    message(STATUS "Fetching googletest")
    FetchContent_Declare(
        googletest
        GIT_REPOSITORY  https://github.com/google/googletest.git
        GIT_TAG         release-1.11.0
    )
    FetchContent_MakeAvailable(googletest)
    add_subdirectory(tests)
endif()
