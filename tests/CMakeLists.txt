add_executable(test_projection test_projection.cpp)
add_test(NAME test_projection COMMAND test_projection)
target_link_libraries(test_projection projection object_depiction gtest gtest_main)

target_include_directories(test_projection PUBLIC
    ${CMAKE_SOURCE_DIR}/game_design)
