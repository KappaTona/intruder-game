#include <gtest/gtest.h>
#include <projection_string.h>
#include <object_depiction/object_depiction.h>
#include <string>


TEST(test_projection_string, test_place_object)
{
    std::unique_ptr<ProjectionInterface> p = std::make_unique<ProjectionString>();
    p->place_object(3*12+2, Depiction::make_character().get());
    p->place_object(2*12+2, Depiction::make_rock().get());

    bool find_default_character = p->projection().find("\\[T]/") != std::string::npos;
    bool find_default_rock = p->projection().find("o") != std::string::npos;

    EXPECT_TRUE(find_default_character) << "Could not place character\n";
    EXPECT_TRUE(find_default_rock) << "Could not place rock\n";
}
