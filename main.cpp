#include <iostream>
#include <memory>
#include "game_design/object_depiction/object_depiction.h"
#include "game_design/projection_string.h"

int main()
{
    std::unique_ptr<ProjectionInterface> p = std::make_unique<ProjectionString>();
    p->place_object(3*12+2, Depiction::make_character().get());
    p->place_object(2*12+2, Depiction::make_rock().get());
    std::cout << p->projection() << std::endl;;
}

