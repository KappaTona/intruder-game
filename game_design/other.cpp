class PlayerOneCharacter : public OneLineObject
{
    constexpr static const char* upper_line = "\\[T]/";
    constexpr static int spacing = 5;
};

struct AdvancedRock
{
    constexpr AdvancedRock() = delete;
    constexpr static const char* upper_line = "/o\\";
    constexpr static const char* bottom_line = " | ";
    constexpr static int spacing = 3;
};


struct PlayerTwoCharacter
{
    constexpr PlayerTwoCharacter() = delete;
    constexpr static const char* upper_line = "\\[+]/";
    constexpr static int spacing = 5;
};

struct NonPlayerCharacter
{
    constexpr NonPlayerCharacter() = delete;
    constexpr static const char* upper_line = "(>_<)";
    constexpr static int spacing = 5;
};


