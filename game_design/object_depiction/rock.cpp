#include "rock.h"


namespace Depiction {
Rock::Rock(int spacing, std::string_view representation) :
        spacing_{spacing}, representation_{representation}
{
}

constexpr Rock::Rock() : spacing_{1}, representation_{"o"}
{
}

std::string_view Rock::upper_line() const 
{
    return representation_;
}

int Rock::spacing() const
{
    return spacing_;
}

std::unique_ptr<OneLineObject> make_rock()
{
    return std::make_unique<Rock>();
}

} //namespace Depiction
