#ifndef rock_h
#define rock_h
#include <string_view>
#include <memory>
#include "one_line_object.h"

namespace Depiction {
class Rock : public OneLineObject
{
public:
    Rock(int spacing, std::string_view representation);
    constexpr Rock();
    std::string_view upper_line() const override;
    int spacing() const override;

private:
    int spacing_;
    std::string_view representation_;
};

std::unique_ptr<OneLineObject> make_rock();
} // namespace Depiction

#endif // rock_h

