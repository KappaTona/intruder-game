#include "character.h"


namespace Depiction
{
Character::Character(int spacing, std::string_view representation) :
        spacing_{spacing}, representation_{representation}
{
}

constexpr Character::Character() : spacing_{5}, representation_{"\\[T]/"}
{
}

std::string_view Character::upper_line() const 
{
    return representation_;
}

int Character::spacing() const
{
    return spacing_;
}

std::unique_ptr<OneLineObject> make_character()
{
    return std::make_unique<Character>();
}


}// namespace Depiction
