#ifndef one_line_object_h
#define one_line_object_h
#include <string_view>

namespace Depiction {
class OneLineObject
{
public:
    virtual ~OneLineObject() = default;
    virtual std::string_view upper_line() const = 0;
    virtual int spacing() const = 0;
};

} //namespace Depiction


#endif //one_line_object_h
