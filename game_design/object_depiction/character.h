#ifndef character_h
#define character_h
#include <string_view>
#include <memory>
#include "one_line_object.h"

namespace Depiction {
class Character : public OneLineObject
{
public:
    Character(int spacing, std::string_view representation);
    constexpr Character();
    std::string_view upper_line() const override;
    int spacing() const override;

private:
    int spacing_;
    std::string_view representation_;
};

std::unique_ptr<OneLineObject> make_character();
} // namespace Depiction

#endif // character_h

