#ifndef projection_string_h
#define projection_string_h
#include "projection_interface.h"


class ProjectionString : public ProjectionInterface
{
public:
    ProjectionString();
    virtual std::string place_object(int, Depiction::OneLineObject*) override;
    std::string projection() const override;

private:
    std::string projection_;
    int projection_line_;
};



#endif //projection_string_h
