#ifndef projection_interface_h
#define projection_interface_h
#include <string>

namespace Depiction
{
    class OneLineObject;
    class TwoLineObject;
}

class ProjectionInterface
{
public:
    virtual ~ProjectionInterface();
    virtual std::string place_object(int, Depiction::OneLineObject*) = 0;
//    virtual std::string place_object(int, Depiction::TwoLineObject*) = 0;
    virtual std::string projection() const = 0;
};


#endif //projection_interface_h
