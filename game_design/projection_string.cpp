#include "projection_string.h"
#include "projection_interface.h"
#include "object_depiction/object_depiction.h"

ProjectionInterface::~ProjectionInterface() = default;

ProjectionString::ProjectionString() :
    projection_{
        "-----------\n"
        "|         |\n"
        "|         |\n"
        "|         |\n"
        "|         |\n"
        "-----------\n"
    },
    projection_line_{12}
{
}

std::string ProjectionString::place_object(
   int start, Depiction::OneLineObject* obj)
{
    return projection_.replace(start, obj->spacing(), obj->upper_line());
}

std::string ProjectionString::projection() const
{
    return projection_;
}
